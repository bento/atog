from arnold import *
import Gaffer,GafferArnold,GafferUI,GafferScene
import IECoreArnold

#INSTALL
# ensure arnold/<VERSION>/python/ is on PYTHONPATH
# ensure mtoa/<VERSION>/shaders/ (and similar) is on ARNOLD_PLUGIN_PATH


#TODO
# - put all new nodes in a box to avoid name clashes and that

#simple gui to allow users to pick from the drop down
class ShaderChooserDialogue( GafferUI.Dialogue ) :

    def __init__( self, title, items, cancelLabel="Cancel", confirmLabel="OK", sizeMode=GafferUI.Window.SizeMode.Automatic, **kw ) :
    
        GafferUI.Dialogue.__init__( self, title,  **kw )
        
        self.__selectionMenu = GafferUI.MultiSelectionMenu()
        self.__selectionMenu.append(items)
        self.__selectionMenu.setSelection(items[0])

        self._setWidget( self.__selectionMenu )

        self._addButton( cancelLabel )
        self.__confirmButton = self._addButton( confirmLabel )
        
    ## Causes the dialogue to enter a modal state, returning the selection
    # if the confirm button was pressed, and False otherwise.
    def waitForConfirmation( self, **kw ) :
    
        button = self.waitForButton( **kw )
        if button is self.__confirmButton :
            return self.__selectionMenu.getSelection()
            
        return False


#recursive function to interrogate and recreate the shader network
def __recreateNetwork(script,node,depth=0,targetNode=None,targetPlug=''):

    nodeName = AiNodeGetName(node)
    nodeType = AiNodeEntryGetName(AiNodeGetNodeEntry(node))

    #check for existing copy of this shader node
    if nodeName in script.keys():
        print '%sExists already [ %s ]' % ('\t'*depth,nodeName)
        gafferNode = script[nodeName]
    else:
        #instanciate node in gaffer
        gafferNode = __createShader(script,nodeName,nodeType,depth)
        #copy values from ai instance of shader to gaffer instance of shader
        __setShaderParams(node,gafferNode)
        
    #connect back to the calling node
    if targetNode and targetPlug:
        print '%sattempting to connect %s.%s to %s' % ('\t'*depth,targetNode,targetPlug,gafferNode)
        targetNode['parameters'][targetPlug].setInput(gafferNode['out'])


    print '%sDepth [ %s ]' % ('\t'*depth,depth)
    print '%sInput node [ %s ]' % ('\t'*depth,nodeName)
    print '%sNode Type [ %s ]' % ('\t'*depth,nodeType)

    #loop over the node's parameters
    piter = AiNodeEntryGetParamIterator(AiNodeGetNodeEntry(node))
    while not AiParamIteratorFinished(piter):
        param = AiParamIteratorGetNext(piter)

        
        #check for incoming connections
        if AiNodeIsLinked(node,AiParamGetName(param)):
            print '%sConnected plug [ %s.%s ]' % ( '\t'*depth,
                                                    AiNodeGetName(node),
                                                    AiParamGetName(param),
                                                 )
            #follow the link
            link = AiNodeGetLink(node,AiParamGetName(param))

            print '%sInput Node [ %s ]' % ('\t'*depth,AiNodeGetName(link))
            __recreateNetwork(script,link,depth+1,gafferNode,AiParamGetName(param))
        

    AiParamIteratorDestroy(piter)


#helper function to actually make a shader
def __createShader(script,name,stype,depth):
    
    #check if node exists already
    if name in script:
        shdr = script[name]

    shdr = GafferArnold.ArnoldShader( name )
    script.addChild( shdr )
    shdr.loadShader( stype )
    
    return shdr


def __setShaderParams(aiInstance,gafferInstance):

    #loop over the node's parameters
    piter = AiNodeEntryGetParamIterator(AiNodeGetNodeEntry(aiInstance))
    while not AiParamIteratorFinished(piter):
        param = AiParamIteratorGetNext(piter)
        paramName = AiParamGetName(param)

        #skip the name as this not treated as a parameter in gaffer
        if paramName != 'name':
 
            if AiParamGetType(param) == AI_TYPE_RGBA:
                result = AiNodeGetRGBA(aiInstance,paramName)
                gafferInstance['parameters'][paramName]['r'].setValue(result.r)
                gafferInstance['parameters'][paramName]['g'].setValue(result.g)
                gafferInstance['parameters'][paramName]['b'].setValue(result.b)
                gafferInstance['parameters'][paramName]['a'].setValue(result.a)

            elif AiParamGetType(param) == AI_TYPE_RGB:
                result = AiNodeGetRGB(aiInstance,paramName)
                gafferInstance['parameters'][paramName]['r'].setValue(result.r)
                gafferInstance['parameters'][paramName]['g'].setValue(result.g)
                gafferInstance['parameters'][paramName]['b'].setValue(result.b)

            elif AiParamGetType(param) == AI_TYPE_INT:
                result = AiNodeGetInt(aiInstance,paramName)
                gafferInstance['parameters'][paramName].setValue(result)

            elif AiParamGetType(param) == AI_TYPE_FLOAT:
                result = AiNodeGetFlt(aiInstance,paramName)
                gafferInstance['parameters'][paramName].setValue(result)

            elif AiParamGetType(param) == AI_TYPE_ENUM:
                val = AiNodeGetInt(aiInstance,paramName)
                enum = AiParamGetEnum(param)
                result = AiEnumGetString(enum,val)
                gafferInstance['parameters'][paramName].setValue(result)

            elif AiParamGetType(param) == AI_TYPE_STRING:
                result = AiNodeGetStr(aiInstance,paramName)
                gafferInstance['parameters'][paramName].setValue(result)

            elif AiParamGetType(param) == AI_TYPE_BOOLEAN:
                result = AiNodeGetBool(aiInstance,paramName)
                gafferInstance['parameters'][paramName].setValue(result)

            elif AiParamGetType(param) == AI_TYPE_VECTOR:
                result = AiNodeGetVec(aiInstance,paramName)
                gafferInstance['parameter'][paramName]['x'].setValue(result.x)
                gafferInstance['parameter'][paramName]['y'].setValue(result.y)
                gafferInstance['parameter'][paramName]['z'].setValue(result.z)

            elif AiParamGetType(param) == AI_TYPE_POINT:
                result = AiNodeGetPnt(aiInstance,paramName)
                gafferInstance['parameter'][paramName]['x'].setValue(result.x)
                gafferInstance['parameter'][paramName]['y'].setValue(result.y)
                gafferInstance['parameter'][paramName]['z'].setValue(result.z)

            elif AiParamGetType(param) == AI_TYPE_POINT2:
                result = AiNodeGetPnt2(aiInstance,paramName)
                gafferInstance['parameter'][paramName]['x'].setValue(result.x)
                gafferInstance['parameter'][paramName]['y'].setValue(result.y)


def __doIt( menu ):

    scriptWindow = menu.ancestor(GafferUI.ScriptWindow)
    script = scriptWindow.scriptNode()

    #IECoreArnold.UniverseBlock is used in place of AiBegin() & AiEnd()
    with IECoreArnold.UniverseBlock():
        AiMsgSetConsoleFlags(AI_LOG_NONE)

        #for some reason defaults to looking for libs called .sog (rather than .so)
        #might have to rename libs to be found
        #update 080514: didn't have this issue in 4.1.3.3 on fedora. maybe os thing..
        for location in os.environ['ARNOLD_PLUGIN_PATH'].split(':'):
            AiLoadPlugins(location)
    
        #ask user for ass file to open
        path = Gaffer.FileSystemPath( os.getcwd() )
        dialogue = GafferUI.PathChooserDialogue(
                                                    path,
                                                    title="Choose ASS file",
                                                    valid=True,
                                                    leaf=True
                                                )
        path = dialogue.waitForPath( parentWindow = scriptWindow )

        if not path or not str(path).endswith('.ass') :
            return
        
        #load the selected ass file
        AiASSLoad(str(path), AI_NODE_ALL)

        #build a list of all the shader nodes in the specified ass
        shaders = []
        siter = AiUniverseGetNodeIterator(AI_NODE_SHADER)
        while not AiNodeIteratorFinished(siter):
            shaders.append(AiNodeGetName(AiNodeIteratorGetNext(siter)))
        AiNodeIteratorDestroy(siter)
        
        #present list of shader nodes to use to pick one for import
        pick = ''

        dialogue = ShaderChooserDialogue('Select shader to import',shaders)
        result = dialogue.waitForConfirmation( parentWindow = scriptWindow )
        pick = result[0]

        rootNode = AiNodeLookUpByName(pick)

        #follow links in shader network out from root,recreating in gfr as we go
        __recreateNetwork(script,rootNode)
        
        #layout any nodes that have been created
        nodeGraphs = scriptWindow.getLayout().editors( GafferUI.NodeGraph )
        if nodeGraphs:
            graph = nodeGraphs[0].graphGadget()
            #TODO - this should just be a set of the nodes we created. how can we track that?
            nodes = Gaffer.StandardSet( graph.getRoot().children( Gaffer.Node.staticTypeId() ))
            graph.getLayout().layoutNodes( graph, nodes )





GafferUI.ScriptWindow.menuDefinition(application).append( "/Tools/ATOG", { "command" : __doIt } )























'''
piter = AiNodeEntryGetParamIterator(AiNodeGetNodeEntry(rootNode))
while not AiParamIteratorFinished(piter):
    param = AiParamIteratorGetNext(piter)
    print AiParamGetName(param)
    #check for incoming connections
    if AiNodeIsLinked(rootNode,AiParamGetName(param)):
        #follow the link
        link = AiNodeGetLink(rootNode,AiParamGetName(param))
        print AiNodeGetName(link)
'''


'''
# Iterate over all shader nodes, 
niter = AiUniverseGetNodeIterator(AI_NODE_SHADER);
while not AiNodeIteratorFinished(niter):
    node = AiNodeIteratorGetNext(niter)
    name = AiNodeGetStr( node, "name" )
    print name

    #now get the parameters for each node and iterate on those
    piter = AiNodeEntryGetParamIterator(AiNodeGetNodeEntry(node))
    while not AiParamIteratorFinished(piter):
        param = AiParamIteratorGetNext(piter)
        print AiParamGetName(param)

    AiParamIteratorDestroy(piter)
AiNodeIteratorDestroy(niter)
'''